const p_user = new Vue({
    el: '#p-user',
    data:{
        usuarios: [],
        nombre: '',
        contraseña: ''
    },
    methods:{
        pasarNombre(){
            this.usuarios.push({
                nombre: this.nombre,
                contraseña: this.contraseña
            });
            this.nombre='';
            this.contraseña='';
            localStorage.setItem('inq-vue', JSON.stringify(this.usuarios));
        }
    },
    created: function(){
        let datosDB = JSON.parse(localStorage.getItem('inq-vue'));
        console.log(datosDB);
        if(datosDB === null){
            this.usuarios = [];
        }else{
            this.usuarios = datosDB;
        }
    }
})
