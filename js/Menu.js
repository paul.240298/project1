const btnToggle = document.querySelector(".toggle-btn");
btnToggle.addEventListener("click", function(){
    document.getElementById("sidebar").classList.toggle("active");
    document.getElementById("panel-prin").classList.toggle("active");
});
const p_user = new Vue({
    el: '#p-user',
    data:{
        usuarios: [],
        nombre: '',
        contraseña: ''
    },
    methods:{
        pasarNombre(){
            this.usuarios.push({
                nombre: this.nombre,
                contraseña: this.contraseña
            });
            this.nombre='';
            this.contraseña='';
        }
    },
    created: function(){
        let datosDB = JSON.parse(localStorage.getItem('inq-vue'));
        if(datosDB === null){
            this.usuarios = [];
        }else{
            this.usuarios = datosDB;
        }
    }
})
